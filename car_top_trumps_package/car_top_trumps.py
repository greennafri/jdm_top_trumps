# Imports
import cars # Imports car data
import random
from random import sample # randomizes assignment of cars
from time import sleep # sleep method makes gameplay more organic

# Initialise car data from cars.py
car_list, attr_list, unit_list = cars.init_cars()

def move():
    '''
    Executes move for each player. The winner of each round goes first.
    Prints out the specs for the first car in the player's hand. Player selects one attribute.
    The function compares the value of the attr to the opponents and determines the winner/loser/draw.
    '''

    # while loop allows for breaks and continues to be used
    while True:

    # Controls who is the current player and opponent
        global rnd_var
        
        if rnd_var is True:
            player = user_1_cards
            player_name = 'Player 1'
            opponent = user_2_cards
            opponent_name = 'Player 2'
        else:
            player = user_2_cards
            player_name = 'Player 2'
            opponent = user_1_cards
            opponent_name = 'Player 1'

        # Indicates which player starts the round
        print(f'''
        _______________
        {player_name}'s turn
        ———————————————
        ''')

        # Prints the specs of the first car in user's hand. abs value printed so that zero_sixty value is positive
        print('_______________________')
        for spec, value in player[0].__dict__.items():
            if isinstance(value, str):
                print(f"{spec.upper()}: {value}")
            else:
                print(f"{spec.upper()}: {abs(value)}")
        print('———————————————————————\n')

        # User input. Selects one of the class attributes. Attribute currently must be typed exactly as shown
        option = input('Please type one of the following attributes: engine, bhp, top_speed, zero_sixty: ')

        # If attr typed incorrectly, loops back to start
        if option.lower() not in attr_list:
            print('Error: Attribute not availble')
            sleep(1)
            continue

        # Sets unit based on selected attr
        if option.lower() == 'engine':
            unit = unit_list[0]
        elif option.lower() == 'bhp':
            unit = unit_list[1]
        elif option.lower() == 'top_speed':
            unit = unit_list[2]
        elif option.lower() == 'zero_sixty':
            unit = unit_list[3]

        sleep(2)

        # Prints the user's selected attr value
        print(f'''
        ________________________________
                    {player_name}:        
            {player[0].name}
            {option.upper()}: {abs(getattr(player[0], option.lower()))} {unit}
        ————————————————————————————————

                        VS          ''')

        sleep(2)

        # Prints opponent's value
        print(f'''
        ________________________________
                    {opponent_name}:        
            {opponent[0].name}
            {option.upper()}: {abs(getattr(opponent[0], option.lower()))} {unit}
        ————————————————————————————————
        ''')

        sleep(2)

        # If player value is more than opponent, deletes the card from user 2 hand and adds to user 1
        if getattr(player[0], option.lower()) > getattr(opponent[0], option.lower()):
            print(f"\n***{player_name} wins***")
            player.append(opponent[0]) # player gets opponent's card. Adds card to back of list
            player.append(player.pop(0)) # Moves player's 1st card to back of list
            opponent.pop(0) # Removes opponent's card from their hand

            if player_name == 'Player 1':
                rnd_var = True
            elif player_name == 'Player 2':
                rnd_var = False

        # If user 2 value is more than user 1, deletes the card from user 1 hand and adds to user 2
        elif getattr(player[0], option.lower()) < getattr(opponent[0], option.lower()):
            print(f"\n***{opponent_name} wins***")
            opponent.append(player[0]) # opponent gets player's card. Adds card to back of list
            opponent.append(opponent.pop(0)) # Moves opponent's 1st card to back of list
            player.pop(0) # Removes player's card from their hand

            if player_name == 'Player 1':
                rnd_var = False
            elif player_name == 'Player 2':
                rnd_var = True

        # If values are the same, nothing happens
        else:
            print("\n***It's a draw***")
            user_1_cards.append(user_1_cards.pop(0)) # Moves 1st card to back of list
            user_2_cards.append(user_2_cards.pop(0)) # Moves 1st card to back of list

        # Prints current hands
        print('\nPlayer 1:')
        for car in user_1_cards:
            print(f"    {car.name}")

        print('\nPlayer 2:')
        for car in user_2_cards:
            print(f"    {car.name}")

        sleep(2)

        break

def main():
    '''
    Initialises game. Assigns cars to each player. Executes moves consecutively until a player has no cars left.
    '''

    # Initialise game data. Global vars used as they are changed outside of main function
    global user_1_cards
    global user_2_cards
    global rnd_var
    rnd_var = True

    # Title card
    print('''
    ___________________________________
    Welcome to Irfaan's JDM Top Trumps!
    ———————————————————————————————————
    ''')

    # Uses random sample module to assign half the total cars to user 1 randomly
    print('Player 1:')
    user_1_cards = sample(car_list, (len(car_list)//2))

    # Prints user 1 car list
    for car in user_1_cards:
        print(f"    {car.name}")
    print('\n')

    # Looks at what cards are in user_1_hand and assigns remaining cars. Prints car list after each append to user 2's hand
    print('Player 2:')
    user_2_cards = []
    for car in car_list:
        if car in user_1_cards:
            pass
        else:
            user_2_cards.append(car)
            print(f"    {car.name}")

    sleep(2)

    # Runs game until either user hand is 0
    while len(user_1_cards) > 0 or len(user_2_cards) > 0:
        move()
        if len(user_1_cards) == 0:
            print('Player 2 won!')
            break
        elif len(user_2_cards) == 0:
            print('Player 1 won!')
            break

main()