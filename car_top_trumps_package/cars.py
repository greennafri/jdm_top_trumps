def init_cars():
    # Car Class 
    class Car:
        def __init__(car, name, engine_size, bhp, top_speed, zero_sixty):
            car.name = name
            car.engine = engine_size
            car.bhp = bhp
            car.top_speed = top_speed
            car.zero_sixty = zero_sixty

    # List of cars. zero_sixty values are negative because the 'bigger' value wins as per move function
    ep3 = Car('Honda Civic EP3 Type R', 1998, 197, 146, -6.6)
    r34 = Car('Nissan Skyline GTR R34', 2568, 276, 155, -5)
    a80 = Car('Toyota Supra A80', 2997, 326, 160, -4.9)
    na2 = Car('Honda NSX NA2 Type S', 3179, 290, 168, -4.5)
    ae86 = Car('Toyota AE86', 1587, 112, 122, -8.3)
    s660 = Car('Honda S660', 658, 63, 87, -11.7)
    dc5 = Car('Honda Integra DC5 Type R', 1998, 187, 145, -6.2)
    wrx = Car('Subaru Impreza WRX STI', 1994, 295, 150, -5)
    s2000 = Car('Honda S2000', 1997, 240, 150, -6)
    rx7 = Car('Mazda RX7', 1308, 237, 156, -5.1)
    z350 = Car('Nissan 350z', 3498, 295, 155, -5.6)
    evox = Car('Mitsubishi Lancer Evo X', 1998, 276, 155, -5.2)
    mx5 = Car('Mazda MX5 1.8i', 1839, 133, 122, -8.7)
    n14 = Car('Nissan Pulsar GTI-R', 1998, 230, 144, -4.9)

    # Lists accessed in main script
    car_list = [ep3, r34, a80, na2, ae86, s660, dc5, wrx, s2000, rx7, z350, evox, mx5, n14]
    attr_list = ['engine', 'bhp', 'top_speed', 'zero_sixty']
    unit_list = ['cc', 'BHP', 'mph', 's']

    return car_list, attr_list, unit_list

init_cars()