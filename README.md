# JDM Top Trumps

Irfaan's JDM Top Trumps! Play with your friends!

## How To Run

Just double-click "run_script.bat". This will open up a CMD window that will display the game

## How To Play

Pick who will be Player 1 and Player 2. By default, player 1 will go first. The winner of each round will start the next until one player has all the cards.

When prompted, pick one of the card attributes you think will beat your opponent (engine, bhp, top_speed or zero_sixty). Currently these are not case sensitive but are character sensitive! 

The script is written so that if anything but an attritubte has been typed, an error will show and the turn will start again.

Try to beat your friends!

## The Script

Car data is stored in "cars.py". This includes the Car Class, a list of cars with unique attributes, list of attributes and list of units. The three lists outputs of the function "init_cars".

The main script is in "car_top_trumps.py". The car data is initialised by pulling the info from "cars.py".

There are two functions, "main" and "move". 

"move" contains all the code necessary to execute a move. A random variable (rnd_var) is set to either True or False at the start of the function to determine the starting player. The move initially prints out the topmost card in the player's hand along with the attributes and values (e.g: Honda s660: engine > 658, bhp > 63, top_speed > 87, zero_sixty > 11).
The player is then prompted to type on of the attributes they think will trump the opponent. The chosen option is used with the "getattr" method to select the value for that car object. This value is compared with the opponents value and three scenarios occur:

1) The player trumps the opponent:
    The player obtains the opponent's card.
    Both cards are put to the back of the player's card list
    The opponent's card is removed from their list using the "pop" method

2) The opponent trumps the player:
    The opponent obtains the player's card
    Both cards are put to the back of the opponent's card list
    The player's card is removed from their list using the "pop" method

3) Both values are the same (a draw):
    The player and opponent's cards are moved to the back of their respective card lists

"main" runs the game. It assigns global variables and initially randomly assigns half the car list to player 1, and the half to player 2. The function then runs the actual game within a while loop. While both card list lengths are more than 0, the loop will run indefinitely. Between each move, the function will check if either card list length is equal to 0. If not, the loop will continue. If so, the player who has all the cards will be declared the winner and the script will break.

## Authors and Acknowledgment

Author: Irfaan Ramjan

Acknowledgement: Gabriel Molina

## Project status
Complete but room for improvement
